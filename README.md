# lolaus

A simple monorepo lifecycle/pipeline tool for running one or more commands on
one or more directories that have diffs compared to an ancestor. The primary use
case is selective CI jobs within a trunk based workflow such Git Flow.

## Usage

lolaus has for optional positional arguments:
`lolaus [glob] [command] [target_ref] [source_ref]`

* glob defaults to **

* command does not default

* target_ref defaults to _next_ if available and then _master_

* source_ref defaults to the current HEAD

Examples:

```shell
  lolaus | parallel "wc -l {} > {}.out"
  lolaus "./tests/* :(top,exclude)**requirements.txt" ls
  lolaus "*/*/package.json" "npm test" release_branch
```

```yaml
docker_images:
  stage: build
  script:
    - lolaus "services/*/Dockerfile" "docker build -t ${PWD##*/} ."

py_tests:
  image: python
  stage: test
  script:
    - lolaus "apps/*/requirements.txt" "python3 tests.py"
```

## Install

`lolaus` is a Bash script and should be fairly universal but not tested outside
of GNU/Linux. OSX users will have a better shell experience (and perhaps life in
general) if they install GNU utilities. Contribs increasing portability or
with other packaging (pip|curl|gem|other) would be great!

```shell
npm install -g lolaus
```

## Design

lolaus is just a convenience shell wrapper of host's git client. The targeted integration branch is used with `git merge-base --fork-point` to determine the common ancestor node (CA). This is one of several common ancestry node
strategies and others could be supported in lolaus, but this seems to provide
the most benefit to the pre-integration, monorepo use case.

`git diff` from the current git ref (Fh) to the CA to determine which files have changes, filtered by the provided glob. From the list of modified files the
command is invoked from a sorted and unique list of directories sequentially via
eval in a loop.

```mermaid
graph LR
  m1(("Sc"))
  ca(("CA"))
  mh(("Mh"))
  f1(("Fc"))
  f2(("Fc"))
  fh(("Fh"))

  m1---ca
  ca-- master ---mh
  ca-- feature ---f1
  ca-. git diff .-fh
  f1---f2
  f2---fh

  classDef gitNode fill:#4ED1A1,stroke:#555,stroke-width:4px;
  class m1,m2,mh,f1,f2,fh,ca gitNode

  classDef gitDiff fill:#4E63D1,stroke:#555,stroke-width:4px;
  class fh,ca gitDiff
```

The design also assumes that each microservice or component is contained in its
own directory and can be identified by some sort of file such as a package.json,
 pom.xml, or requirements.txt via the glob.

There is no parallelism, lolaus is intended to be used pre-integration where
only one or two components that are effected in the merge request. In cases
where target_ref and source_ref defaults work, the command parameter can be
omitted and the list of effected directors can be piped to parallel.
